from tkinter import *
from PIL import Image, ImageTk
import requests
import time
import json
import codecs
import os
from multiprocessing import Pool, TimeoutError, Queue
from multiprocessing.dummy import Pool as ThreadPool 
from bs4 import BeautifulSoup
from threading import *

class Login(Frame):

	def __init__(self, master = None):
		Frame.__init__(self,master)



		self.master = master

		self.init_window()
		root.geometry("550x500")

	def init_window(self):
		self.master.title("PREME SPEED CART V0.0.0.1 ALPHA")
		self.pack(fill = BOTH,expand = 1)

		load = Image.open('SpeedCart.png')
		render = ImageTk.PhotoImage(load)
		img = Label(self,image=render)
		img.image = render
		img.place(x=0,y=100)
		quitButton = Button(self, text = "Login", command=self.client_exit)

		quitButton.place(x=250,y=400)



	def client_exit(self):
		self.destroy()
		MainWindow(root)





 















class MainWindow(Frame):
	def __init__(self, master = None):
		Frame.__init__(self,master)



		self.master = master

		self.init_window()
		

	def init_window(self):
		
		root.geometry("1280x720")
		self.master.title("PREME SPEED CART V0.0.0.1 ALPHA")
		self.pack(fill = BOTH,expand = 1)
		menu = Menu(self.master)
		self.master.config(menu=menu)

		file = Menu(menu)
		file.add_command(label = "Exit",command=self.quit)
		menu.add_cascade(label='File', menu = file)
		preferences = Menu(menu)
		preferences.add_command(label = "Size Preferences")
		preferences.add_command(label = "Payment Preferences",command = self.payment_profiles)
		menu.add_cascade(label = "Preferences",menu = preferences)
		view = Menu(menu)
		view.add_command(label = "View Supreme Stock")
		menu.add_cascade(label = "View",menu = view)
		start = time.time()
		response = requests.get('http://www.supremenewyork.com/shop')
		ping = time.time()-start
		ping = ping*100
		if(response.status_code == 200):
			 sup_response = "OK!"
		else:
			sup_response = "Bad Connection!"


		
		sup_label_connection = Label(self,text = "Connection to Supreme New York: "+sup_response )
		sup_label_connection.config(font=("Courier",12))
		sup_label_connection.pack(side = "top", fill = "y")
		ping_label_connection = Label(self,text = "Ping Response: "+str(ping) + " ms")
		ping_label_connection.config(font=("Courier",12))
		ping_label_connection.pack(side = "top", fill = "y")
		title_dropList = Label(self,text = "This Weeks Drop")
		title_dropList.config(font=("Courier",12))
		title_dropList.pack(side = "top", fill = "y")
		self.listNodes = Listbox(self, width=50, height=20, font=("Courier", 12))
		self.listNodes.pack(side="top", fill="y")
		self.listNodes.insert(0,"")
		self.item_page()
		self.listNodes.bind("<Double-Button-1>",self.droplist_onclick)
		self.item_info = Label(self,text = "Item Specs: ")
		self.item_info.config(font=("Courier",12))
		self.item_info.pack(anchor = W)
		payment_label = Label(self,text = "Payment Profile: ")
		payment_label.config(font=("Courier",12))
		payment_label.pack(side = "left",anchor = W)
		self.pay_variable = StringVar(self)
		self/pay_variable.set("Profile")
		self.json_files = [pos_json for pos_json in os.listdir('payloads') if pos_json.endswith('')]
		
		self.options = self.json_files

		self.pay_options = OptionMenu(self,self.pay_variable, *self.options, command = lambda event : self.printOptions(event))
		self.pay_options.pack(side = "left", anchor = W)
		
		self.threads=[]
		dummy_label = Label(self,text = "     ")
		dummy_label.pack(side = "left",anchor = W)
		style_label = Label(self,text = "Style Profile: ")
		style_label.config(font=("Courier",12))
		style_label.pack(side = "left",anchor = W)
		style_variable = StringVar(self)
		style_variable.set("Profile")
		style_options = {"BOGOS","Accesory","Clothes"}
		self.style_options = OptionMenu(self,style_variable, *style_options)
		self.style_options.pack(side = "left", anchor = W)
		dummy_label2 = Label(self,text = "                                      ")
		dummy_label2.pack(side = "left",anchor = W)
		t1 = Thread(target=self.showConsole, args=[])
		self.pool = ThreadPool(4)
		self.hitlick = Button(self,text = "Start Cookin",command = self.createConsoleWorker)
		
		self.hitlick.pack(anchor = W)
	def droplist_onclick(self,event):
		self.item_info.config(text ="Item Specs:"  + self.listNodes.get(self.listNodes.curselection()[0]))

	def printOptions(self,event):
		print("hello world")

	def payment_profiles(self):
		payment_profiles = Toplevel(root)
		payment_profiles.geometry("800x600")
		


		self.full_name = Entry(payment_profiles)
		self.full_name.grid(row = 0, column = 1)
		full_name_label = Label(payment_profiles,text = "Full Name:")
		full_name_label.config(font=("Courier",12))
		full_name_label.grid(row = 0, column = 0)

		self.phone = Entry(payment_profiles)
		self.phone.grid(row = 1, column = 1)
		phone_label = Label(payment_profiles,text = "Phone(ex: XXX-XXX-XXXX):")
		phone_label.config(font=("Courier",12))
		phone_label.grid(row = 1, column = 0)


		self.email = Entry(payment_profiles)
		self.email.grid(row = 2, column = 1)
		email_label = Label(payment_profiles,text = "Email:")
		email_label.config(font=("Courier",12))
		email_label.grid(row = 2, column = 0)


		self.bus_addr = Entry(payment_profiles)
		self.bus_addr.grid(row = 3, column = 1)
		bus_addr_label = Label(payment_profiles,text = "Street Address:")
		bus_addr_label.config(font=("Courier",12))
		bus_addr_label.grid(row = 3, column = 0)

		self.bus_addr_2 = Entry(payment_profiles)
		self.bus_addr_2.grid(row = 4, column = 1)
		bus_addr_label_2 = Label(payment_profiles,text = "Street Address 2:")
		bus_addr_label_2.config(font=("Courier",12))
		bus_addr_label_2.grid(row = 4, column = 0)


		self.bus_zip = Entry(payment_profiles)
		self.bus_zip.grid(row = 5, column = 1)
		bus_zip_label = Label(payment_profiles,text = "Zip Code:")
		bus_zip_label.config(font=("Courier",12))
		bus_zip_label.grid(row = 5, column = 0)


		self.bus_city = Entry(payment_profiles)
		self.bus_city.grid(row = 6, column = 1)
		bus_city_label = Label(payment_profiles,text = "City:")
		bus_city_label.config(font=("Courier",12))
		bus_city_label.grid(row = 6, column = 0)


		self.bus_state = Entry(payment_profiles)
		self.bus_state.grid(row = 7, column = 1)
		bus_state_label = Label(payment_profiles,text = "State:")
		bus_state_label.config(font=("Courier",12))
		bus_state_label.grid(row = 7, column = 0)



		self.bus_country = Entry(payment_profiles)
		self.bus_country.grid(row = 8, column = 1)
		bus_country_label = Label(payment_profiles,text = "Country:")
		bus_country_label.config(font=("Courier",12))
		bus_country_label.grid(row = 8, column = 0)

		
		

		self.card_number = Entry(payment_profiles)
		self.card_number.grid(row = 9, column = 1)
		card_number_label = Label(payment_profiles,text = "Card Number(ex: XXXX XXXX XXXX XXXX):")
		card_number_label.config(font=("Courier",12))
		card_number_label.grid(row = 9, column = 0)


		self.card_type = Entry(payment_profiles)
		self.card_type.grid(row = 10, column = 1)
		card_type_label = Label(payment_profiles,text = "Card Type(ex : visa) :")
		card_type_label.config(font=("Courier",12))
		card_type_label.grid(row = 10, column = 0)


		self.card_exp_month = Entry(payment_profiles)
		self.card_exp_month.grid(row = 11, column = 1)
		card_exp_month_label = Label(payment_profiles,text = "Expiration Month(ex: 02):")
		card_exp_month_label.config(font=("Courier",12))
		card_exp_month_label.grid(row = 11, column = 0)



		self.card_exp_year = Entry(payment_profiles)
		self.card_exp_year.grid(row = 12, column = 1)
		card_exp_year_label = Label(payment_profiles,text = "Expiration Year(ex: 2020):")
		card_exp_year_label.config(font=("Courier",12))
		card_exp_year_label.grid(row = 12, column = 0)



		self.card_cvv = Entry(payment_profiles)
		self.card_cvv.grid(row = 13, column = 1)
		card_cvv_label = Label(payment_profiles,text = "CVV:")
		card_cvv_label.config(font=("Courier",12))
		card_cvv_label.grid(row = 13, column = 0)


		self.profile_name = Entry(payment_profiles)
		self.profile_name.grid(row = 14, column = 1)
		profile_name_label = Label(payment_profiles,text = "Profile Name:")
		profile_name_label.config(font=("Courier",12))
		profile_name_label.grid(row = 14, column = 0)

		save = Button(payment_profiles,text = "Save Profile", command = self.savePayment)
		save.grid(row =14, column = 2)



	def startbot(self):
		os.system('py python_speedcart.py')






	def showConsole(self,item_name,checkoutPayload,size,styleKeyword):

		
		console = Toplevel(self)
		console.geometry("500x500")
		console_label = Label(console,text = "Console Output")
		console_label.config(font=("Courier",12))
		console_label.pack()
		consoleLog = Listbox(console, width=50, height=20, font=("Courier", 12))
		consoleLog.pack(side="top", fill="y")
		consoleLog.insert(0,"")
		var = 1
		while(1):
			var = var+1
			consoleLog.insert(END, "Hunting for Item: " + item_name)
			time.sleep(1)
			consoleLog.yview(END)







	def createConsoleWorker(self):
		args = []
		passed_item_name = self.listNodes.get(self.listNodes.curselection()[0])
		args.append(passed_item_name)
		json_file = open('/payloads/'+ self.pay_variable).read()
		json_payment_payload = json.loads(json_file)
		args.append(json_payment_payload)
		t = Thread(target = self.showConsole,args = [passed_item_name])

		self.threads.append(t)
		t.start()







	def savePayment(self):
		checkoutPayload={
        'store_credit_id':    '',      
        'from_mobile':              '1',
        'cookie-sub':               '%7B%22'+'str(size_code)'+'%22%3A1%7D',       # cookie-sub: eg. {"VARIANT":1} urlencoded
        'same_as_billing_address':  '1',                                    
        'order[billing_name]':      self.full_name.get(),                              # FirstName LastName
        'order[email]':             self.email.get(),                    # email@domain.com
        'order[tel]':               self.phone.get(),                           # phone-number-here
        'order[billing_address]':   self.bus_addr.get(),                        # your address
        'order[billing_address_2]': '',
        'order[billing_zip]':       self.bus_zip.get(),                                  # zip code
        'order[billing_city]':      self.bus_city.get(),                          # city
        'order[billing_state]':     self.bus_state.get(),                                     # state
        'order[billing_country]':   self.bus_country.get(),                                    # country
        'store_address':            '1',                                
        'credit_card[type]':        self.card_type.get(),                                   # master or visa
        'credit_card[cnb]':         self.card_number.get(),                    # credit card number
        'credit_card[month]':       self.card_exp_month.get(),                                     # expiration month
        'credit_card[year]':        self.card_exp_year.get(),                                   # expiration year
        'credit_card[vval]':        self.card_cvv.get(),                                    # cvc/cvv
        'order[terms]':             '0',
        'order[terms]':             '1'                
    	}
		


		with open('payloads/'+self.profile_name.get(), 'wb') as f:
			json.dump(checkoutPayload, codecs.getwriter('utf-8')(f), ensure_ascii=False)

		self.pay_options["menu"].add_command(label = self.profile_name.get())
		alert = Toplevel(root)
		alert_label = Label(alert, text = "Payment profile saved! ")
		alert_label.config(font=("Courier",12))
		alert_label.pack()
		


















	def item_page(self):
		count = 0
		s = requests.session()
		response = s.get('https://www.supremecommunity.com/season/spring-summer2018/droplists/')
		soup = BeautifulSoup(response.text, 'html.parser')
		mydivs = soup.find_all('a', href=True)
		response = s.get("https://www.supremecommunity.com" +mydivs[21]['href'] )
		soup = BeautifulSoup(response.text, 'html.parser')
		tags = soup.findAll("div", {"class" : "card__body"})
		for a in tags:
			mystring =a.text 
			mystring = mystring.replace('\n', ' ').replace('\r', '')
			self.listNodes.insert(count,mystring)
			count=count+1


	def quit(self):
		quit()
		



root = Tk()

app = Login(root)

root.mainloop()