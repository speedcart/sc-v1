import os
from tkinter import *
from subprocess import Popen, PIPE
root = Tk()
text = Text(root)
text.pack()

def ls_proc():
    return Popen(['dir'], stdout=PIPE)

with ls_proc() as p:
    if p.stdout:
        for line in p.stdout:
            text.insert(END, line)
    if p.stderr:
        for line in p.stderr:
            text.insert(END, line)

root.mainloop()